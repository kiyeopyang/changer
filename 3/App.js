import React, { Component, Fragment } from 'react';
import pdfjsLib from 'pdfjs-dist';
import update from 'react-addons-update';
import TextBox from './TextBox';

// pdfjsLib.GlobalWorkerOptions.workerSrc =
//   '../../build/webpack/pdf.worker.bundle.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum : 1,
      pdfDoc: null,
      pageRendering: false,
      pageNumPending: null,
      scale: 1.5,
      serverUrl: '',
      fetchUrl: '',
      uploadEndpoint: '',
      getEndpoint: '',
      getResult: [],
      fileDataForUpload: null,
      
      inputs: [],

      editLayerDownCoords: null,
      editLayerCurrentCoords: null,
    };
    this.editCanvas = React.createRef();
    this.editLayer = React.createRef();
    this.canvas = React.createRef();
    this.fileSelection = React.createRef();
    this.uploadFileSelection = React.createRef();
  }
  componentDidUpdate() {
    this.drawEditCanvas();
  }
  componentDidMount() {
    this.renderPdf({ url: 'b.pdf' });
    
  }
  onSelectFile = e => {
    const fileReader = new FileReader();
    fileReader.onloadend = e => {
      this.renderPdf({ data: e.target.result });
    }
    const file = e.target.files[0];
    fileReader.readAsArrayBuffer(file);
  }
  renderPdf = ({ url, data }) => {
    pdfjsLib
      .getDocument(url ? url : { data })
      .then((pdfDocument) => {
        this.setState({ pdfDoc: pdfDocument });
        this.renderPage(1);
      })
      .catch((reason) => {
        console.error('Error: ' + reason);
      });
  }
  renderPage(num) {
    this.setState({ pageRendering: true });
    this.state.pdfDoc.getPage(num).then((page) => {
      console.log(page);
      const { height, width, ...rest } = page.getViewport(this.state.scale);
      this.canvas.current.height = height;
      this.canvas.current.width = width;

      //
      this.editCanvas.current.height = height;
      this.editCanvas.current.width = width;
     
      //
  
      const renderContext = {
        canvasContext: this.canvas.current.getContext('2d'),
        viewport: { height, width, ...rest },
      };
      const renderTask = page.render(renderContext);
  
      renderTask.promise.then(() => {
        // render success
        this.setState((state) => {
          const { inputs } = state;
          if (!inputs[num - 1]) {
            inputs[num - 1] = [];
          }
          return {
            pageRendering: false,
            pageNum: num,
            inputs,
            pdf: {
              height,
              width,
            },
          }
        });
        if (this.state.pageNumPending !== null) {
          this.renderPage(this.state.pageNumPending);
          this.setState({ pageNumPending: null });
        }
      });
    });
  }
  queueRenderPage = (num) => {
    if (this.state.pageRendering) {
      this.setState({ pageNumPending: num });
    } else {
      this.renderPage(num);
    }
  }
  onPrevPage = () => {
    if (this.state.pageNum <= 1) {
      return;
    }
    const pageNum = this.state.pageNum - 1;
    // this.setState({ pageNum });
    this.queueRenderPage(pageNum);
  }
  onNextPage = () => {
    if (this.state.pageNum >= this.state.pdfDoc.numPages) {
      return;
    }
    const pageNum = this.state.pageNum + 1;
    // this.setState({ pageNum });
    this.queueRenderPage(pageNum);
  }
  fetchPdf = (url) => {
    fetch(url)
      .then(res => res.arrayBuffer())
      .then((data) => {
        this.renderPdf({ data });
      });
  }
  upload = (url) => {
    const data = new FormData();
    data.append('file', this.state.fileDataForUpload);
    data.append('text', 'ha');
    data.append('test', JSON.stringify({a:2, b:'a'}));
    fetch(url, {
      method: 'POST',
      body: data,
    }).then(
      response => response.json()
    ).then(
      success => console.log(success)
    ).catch(
      error => console.log(error)
    );
  }
  get = (url) => {
    fetch(url, )
      .then(res => res.json())
      .then((data) => {
        console.log(data);
        this.setState({
          getResult: data,
        });
      });
  }
  handleEditCanvasEvents = name => (e) => {
    e.preventDefault();
    e.stopPropagation();
    const { x, y, ...rest } = this.editCanvas.current.getBoundingClientRect();
    const relativeX = Math.round(e.clientX - x);
    const relativeY = Math.round(e.clientY - y);
    switch (name) {
      case 'down': {
        if (!this.isThisCoordsInvalid({ x: relativeX, y: relativeY })) {
          const editLayerDownCoords = { x: relativeX, y: relativeY };
          this.setState({
            editLayerDownCoords,
          });
        }
        break;
      }
      case 'up': {
        if (this.state.editLayerDownCoords) {
          this.clearEditCanvas();
          let { 
            editLayerDownCoords,
            editLayerCurrentCoords,
            inputs,
            pageNum,
          } = this.state;
          const isXreversed = editLayerDownCoords.x > editLayerCurrentCoords.x;
          const isYreversed = editLayerDownCoords.y > editLayerCurrentCoords.y;
          const textBox = {
            type: 'text',
            id: new Date().getTime(),
            x: isXreversed ? editLayerCurrentCoords.x : editLayerDownCoords.x,
            y: isYreversed ? editLayerCurrentCoords.y : editLayerDownCoords.y,
            width: (editLayerCurrentCoords.x - editLayerDownCoords.x) * (isXreversed ? -1 : 1),
            height: (editLayerCurrentCoords.y - editLayerDownCoords.y) * (isYreversed ? -1 : 1),
          };
          
          if (this.checkIsValidPos({
            x: textBox.x,
            y: textBox.y,
            width: textBox.width,
            height: textBox.height,
          }) && textBox.width > 50 && textBox.height > 15) {
            inputs[pageNum - 1] = inputs[pageNum - 1].concat([textBox]);
          } else {
            console.error('INVALID');
          }
          this.setState({
            editLayerDownCoords: null,
            editLayerCurrentCoords: null,
            inputs,
          });
        }
        break;
      }
      case 'move': {
        if (this.state.editLayerDownCoords) {
          // this.setState({
          //   editLayerCurrentCoords: { x: relativeX, y: relativeY },
          // });
          this.setState({
            editLayerCurrentCoords: { x: relativeX, y: relativeY },
          });
        }
        break;
      }
      default:
    }
    // const { editLayer } = this;
    // const rect = editLayer.getBoundingClientRect();
    // this.setState({ inputs: this.state.inputs.concat({
    //   arc: { x: e.clientX - rect.left, y: e.clientY - rect.top },
    //   text: { x: e.clientX - rect.left, y: e.clientY - rect.top + 4, value: this.state.inputs.length + 1, label: '' },
    //   style: "#FF0000",
    // })});
  }
  checkIsValidPos = (toBeMade = { x: 0, y: 0, width: 0, height: 0 }) => {
    const { inputs, pageNum } = this.state;
    let isValid = true;
    inputs[pageNum - 1].forEach((input) => {
      // 필드 안에 만들 필드의 좌표가 포함 되어있나
      const { x, y, width, height } = toBeMade;
      const pos = [{ x, y }, { x: x + width, y }, { x, y: y + height }, { x: x + width, y: y + height }];
      if (pos.some(({ x, y }) => this.isThisCoordsInRect({ x, y }, { x: input.x, y: input.y, width: input.width, height: input.height }))) {
        isValid = false;
      }
      // 만들 필드의 안에 필드의 좌표들이 포함되어 있나
      const inputPos = [{ x: input.x, y: input.y }, { x: input.x + input.width, y: input.y }, { x: input.x, y: input.y + input.height }, { x: input.x + input.width, y: input.y + input.height }];
      if (inputPos.some(({ x, y }) => this.isThisCoordsInRect({ x, y }, { x: toBeMade.x, y: toBeMade.y, width: toBeMade.width, height: toBeMade.height }))) {
        isValid = false;
      }
    })
    return isValid;
  }
  isThisCoordsInvalid = (coords) => {
    const { inputs, pageNum } = this.state;
    if (inputs[pageNum - 1].length && inputs[pageNum - 1].some((input) => this.isThisCoordsInRect(coords,input))) {
        return true;
    }
    return false;
  }
  isThisCoordsInRect = (coords = { x: 0, y: 0 }, rect = { x: 0, y: 0, width: 0, height: 0 }) => {
    if (coords.x >= rect.x && coords.y >= rect.y && coords.x <= rect.x + rect.width && coords.y <= rect.y + rect.height) {
      return true;
    }
    return false;
  };
  clearEditCanvas = () => {
    const canvas = this.editCanvas.current;
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  drawEditCanvas = () => {
    const {
      editLayerDownCoords,
      editLayerCurrentCoords,
    } = this.state;
    if (editLayerDownCoords && editLayerCurrentCoords) {
      this.clearEditCanvas();
      const canvas = this.editCanvas.current;
      const ctx = canvas.getContext('2d');
      ctx.strokeRect(
        editLayerDownCoords.x,
        editLayerDownCoords.y,
        editLayerCurrentCoords.x - editLayerDownCoords.x, // width
        editLayerCurrentCoords.y - editLayerDownCoords.y, // height
      );
    };
  }
  createPdf = () => {
    const html = `
    <html>
      <head>
        <style>
          html, body {
            margin: 0;
            padding: 0;
          }
        </style>
      </head>
      <body>
        ${this.editLayer.current.outerHTML}
      </body>
    </html>
    `
    console.log(html);
  }
  render() {
    const { pdf, inputs, pageNum } = this.state;
    console.log(this.state.inputs);
    return (
      <div>
        <div style={{textAlign: 'center'}}>
          <h4>페이지 이동</h4>
          <button onClick={this.onPrevPage}>{`<`}</button>
          <button onClick={this.onNextPage}>{`>`}</button>
          <p>{`Page: ${this.state.pageNum}`}</p>
          <button onClick={this.createPdf}>{`생성`}</button>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          {
            pdf ?
              <Fragment>
                <div
                  ref={this.editLayer}
                  style={{ border: '1px solid black', position: 'absolute', width: pdf.width, height: pdf.height, }}
                >
                {
                  inputs[pageNum - 1].map((input, i) => (
                    <TextBox
                      onChange={editorState => this.setState((state) => {
                        input.editorState = editorState;
                        return { inputs: update(state.inputs, { [pageNum - 1]: { $splice: [[i, 1, input]] } } ) };
                      })}
                      editorState={input.editorState}
                      key={input.id}
                      x={input.x}
                      y={input.y}
                      width={input.width}
                      height={input.height}
                    />
                  ))
                }
              </div>
            </Fragment>
            : null
          }
          <canvas
            style={{ border: '1px solid black', position: 'absolute' }}
            onMouseDown={this.handleEditCanvasEvents('down')}
            onMouseUp={this.handleEditCanvasEvents('up')}
            onMouseMove={this.handleEditCanvasEvents('move')}
            ref={this.editCanvas}
            id="editCanvas"
          />
          <canvas style={{ border: '1px solid black' }} ref={this.canvas} id="canvas" />
        </div>
      </div>
    );
  }
}

export default App;
