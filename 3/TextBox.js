import React from 'react';
import ReactDOM from 'react-dom';
import {Editor, EditorState} from 'draft-js';

class TextBox extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props.editorState);
    this.state = {editorState: this.props.editorState || EditorState.createEmpty()};
    this.onChange = (editorState) => {
      this.props.onChange(editorState);
      this.setState({editorState});
    }
    this.setEditor = (editor) => {
      this.editor = editor;
    };
    this.focusEditor = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };
  }

  componentDidMount() {
    this.focusEditor();
  }
  // height 넘어서는 글자 못쓰도록 설정
  render() {
    const { x, y, width, height } = this.props;
    return (
      <div style={{zIndex: 10, left: x, top: y, width, height, ...styles.editor}} onClick={this.focusEditor}>
        <Editor
          ref={this.setEditor}
          editorState={this.state.editorState}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

const styles = {
  editor: {
    position: 'absolute',
    border: '1px solid gray',
    cursor: 'text'
  },
};

export default TextBox;
