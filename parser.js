const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('C:/Users/Kiyeop/Project/x/test.html', {waitUntil: 'networkidle0'});
  await page.emulateMedia('screen');
  await page._emulationManager._client.send(
    'Emulation.setDefaultBackgroundColorOverride',
    { color: { r: 0, g: 0, b: 0, a: 0 } }
  );
  await page.pdf({path: 'hn3.pdf', printBackground: true, width: 595,height: 841});

  await browser.close();
})();
