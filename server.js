const handler = require('serve-handler');
const http = require('http');

const server = http.createServer((request, response) => {
  return handler(request, response, {
    "public": "build",
    "redirects": [
      { "source": "!/", "destination": "/", "type": 302 }
    ]
  });
})
 
const port = process.env.PORT || 3001;
server.listen(port, () => {
  console.log(`Running at http://localhost:${port}`);
});
