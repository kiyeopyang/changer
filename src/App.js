import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  withRouter,
  Route,
  Switch,
} from 'react-router-dom';
import MessageBar from './components/MessageBar';
import { auth } from './data/auth/actions';
import Loader from './components/Loader';
import LandingPage from './scenes/LandingPage';
import ManagerPage from './scenes/ManagerPage';
import Submit from './scenes/Submit';

class App extends Component {
  constructor(props) {
    super(props);
    this.props.requestAuth();
  }
  render() {
    const { auth } = this.props;
    if (auth.loading) return null;
    return (
      <React.Fragment>
        <Switch>
          <Route path="/for-submit/:id" component={Submit} />
          <Route path="/" component={auth.response ? ManagerPage : auth.loading ? Loader : LandingPage} />
        </Switch>
        <MessageBar />
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  auth: state.data.auth,
});
export default withRouter(connect(mapStateToProps, {
  requestAuth: auth.request,
})(App));
