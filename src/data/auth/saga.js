import {
  actions,
  auth,
} from './actions';
import request from '../../modules/request';
import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* get({ params }) {
  const { response } = yield call(
    request,
    `/api/user`,
    {
      headers: {
        credentials: 'include',
      },
    },
  );
  console.log(response);
  if (response) yield put(auth.success(params, response));
  else yield put(auth.failure(params));
}
export default [
  takeLatest(actions['REQUEST'], get),
];
