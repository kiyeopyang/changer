import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import data from './data/reducer';
import LandingPage from './scenes/LandingPage/reducer';
import ManagerPage from './scenes/ManagerPage/reducer';
import Submit from './scenes/Submit/reducer';
import Editor from './scenes/Editor/reducer';

export default history => combineReducers({
  router: connectRouter(history),
  data,
  LandingPage,
  ManagerPage,
  Submit,
  Editor,
});
