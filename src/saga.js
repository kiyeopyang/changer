import {
  all,
} from 'redux-saga/effects';
import saga from './data/saga';
import LandingPage from './scenes/LandingPage/saga';
import ManagerPage from './scenes/ManagerPage/saga';
import Submit from './scenes/Submit/saga';
import Editor from './scenes/Editor/saga';

export default function* rootSaga() {
  yield all([
    ...saga,
    ...LandingPage,
    ...ManagerPage,
    ...Submit,
    ...Editor,
  ]);
}
