import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = {
  layout: {
    display: 'flex',
  },
};
const Layout = ({ classes, children }) => (
  <div className={classes.layout}>
    { children }
  </div>
)
export default withStyles(styles)(Layout);
