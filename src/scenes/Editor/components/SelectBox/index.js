import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

const styles = {
  labelWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.5,
    zIndex: 0,
  },
  // cannot know this classes in server
  selectBox: {
    position: 'absolute',
    border: '1px solid green',
    padding: 1,
    cursor: 'pointer',
  },
};
class SelectBox extends React.Component {
  render() {
    const { classes, x, y, width, height, handleClick, label }  = this.props;
    return (
      <div
        className={classes.selectBox}
        style={{ left: x, top: y, width, height }}
        onClick={handleClick}
      >
        <div className={classes.labelWrapper}>
          <Typography variant="h6">
            {label}
          </Typography>
        </div>
      </div>
    );
  }
}


export default withStyles(styles)(SelectBox);
