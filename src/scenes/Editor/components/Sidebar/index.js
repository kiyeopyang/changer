import React, {Fragment} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
import Divider from '@material-ui/core/Divider';
import update from 'react-addons-update';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import classnames from 'classnames';

const styles = theme => ({
  list: {
    width: 260,
    background: 'rgb(34, 46, 62)',
    // position: 'static',
    boxShadow: '0px 8px 10px 5px rgba(0, 0, 0, 0.42), 0px 16px 24px 2px rgba(0, 0, 0, 0.12), 0px 6px 30px 5px rgba(0, 0, 0, 0.2)',
  },
  white: {
    color: 'white',
  },
  backgroundGrey: {
    background: 'grey',
    '&:hover': {
      backgroundColor: 'grey',
    },
  },
  margin: {
    margin: theme.spacing.unit,
  },
  cssLabel: {
    color: 'white',
    '&$cssFocused': {
      color: '#e91e63',
    },
  },
  cssFocused: {
    color: 'white',
    borderBottomColor: 'white',
  },
  cssUnderline: {
    color: 'white',
    borderBottomColor: 'white',
    '&:after': {
      color: 'white',
      borderBottomColor: '#e91e63',
    },
    '&:before': {
      color: 'white',
      borderBottomColor: 'white',
    }
  },
  outlineRoot: {
    color: 'white',
    '&:after': {
      borderColor: '#e91e63',
    },
    '&:before': {
      borderColor: '#e91e63',
    }
  },
  outline: {
    borderColor: 'rgba(255, 255, 255, 0.23)',
    '&:after': {
      color: 'white',
      borderBottomColor: '#e91e63',
    },
    '&:before': {
      color: 'white',
      borderBottomColor: 'white',
    }
  }
});
class Sidebar extends React.Component {
  render() {
    const {
      classes, showAuto, handleClick, isNew, inputs, pageNum, submitted, createType, onChange
    } = this.props;
    console.log(showAuto);
    return (
      <Drawer
        variant="permanent"
        anchor={"right"}
        open={this.props.open}
        onClose={this.props.handleDrawerToggle}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
        classes={{
          paper: classes.list,
        }}
      >
        <div ref="sidebarWrapper">
          <List >
            <ListItem onClick={() => handleClick('pdf')} button className={classes.white}>
              <ListItemIcon className={classes.white}>
                <Icon>picture_as_pdf</Icon>
              </ListItemIcon>
              <ListItemText
                primary={"PDF 다운로드"}
                disableTypography={true}
              />
            </ListItem>
            {
              isNew ?
              <ListItem onClick={() => handleClick('save')} button className={classes.white}>
                <ListItemIcon className={classes.white}>
                  <Icon>save</Icon>
                </ListItemIcon>
                <ListItemText
                  primary={"폼 저장하기"}
                  disableTypography={true}
                />
              </ListItem> :
              <ListItem onClick={() => handleClick('link')} button className={classes.white}>
                <ListItemIcon className={classes.white}>
                  <Icon>move_to_inbox</Icon>
                </ListItemIcon>
                <ListItemText
                  primary={"링크 복사"}
                  disableTypography={true}
                />
              </ListItem>
            }
            <ListItem onClick={() => handleClick('delete')} button className={classes.white}>
              <ListItemIcon className={classes.white}>
                <Icon>delete</Icon>
              </ListItemIcon>
              <ListItemText
                primary={"문서 삭제"}
                disableTypography={true}
              />
            </ListItem>
          </List>
          <Divider className={classes.white}/>
          <List>

            {
              isNew &&
              <Fragment>
                <ListItem>
                  <FormControl variant="outlined" fullWidth>
                    <InputLabel
                      htmlFor="outlined-age-native-simple"
                      classes={{
                        root: classes.cssLabel,
                        focused: classes.cssFocused,
                        disabled: classes.cssLabel,
                      }}
                    >
                    생성 타입
                    </InputLabel>
                    <Select
                      fullWidth
                      value={createType}
                      onChange={e => onChange(e.target.value)}
                      input={
                        <OutlinedInput
                          name="outlined-age"
                          labelWidth={0}
                          id="outlined-age-native-simple"
                          classes={{
                            root: classes.outlineRoot,
                            notchedOutline: classes.outline,
                          }}
                        />
                      }
                    >
                      <MenuItem value={'text'}>텍스트</MenuItem>
                      <MenuItem value={'image'}>이미지</MenuItem>
                    </Select>
                  </FormControl>
                </ListItem>
                {/* <ListItem
                  onClick={() => handleClick('showAuto')}
                  button
                  className={classnames(classes.white,
                    { [classes.backgroundGrey]: showAuto })}
                >
                  <ListItemIcon className={classes.white}>
                    <Icon>pageview</Icon>
                  </ListItemIcon>
                  <ListItemText
                    classes={{
                      primary: classes.white,
                      secondary: classes.white,
                    }}
                    primary={"폼 자동 인식"}
                  />
                </ListItem> */}
              </Fragment>
            }
          {
            inputs[pageNum -1] && inputs[pageNum - 1].map((o, i) => {
              console.log(o);
              const isSubmiited = typeof o.text === 'string';
              return (
                <ListItem key={`page${pageNum - 1}, ${i}`}>
                  <FormControl fullWidth>
                    <InputLabel
                      htmlFor={`page${pageNum - 1}, ${i}`}
                      classes={{
                        root: classes.cssLabel,
                        focused: classes.cssFocused,
                        disabled: classes.cssLabel,
                      }}
                    >
                      { isSubmiited ? o.label : `${i+1}번 폼 라벨` }
                    </InputLabel>
                    <Input
                      fullWidth
                      id={`page${pageNum - 1}, ${i}`}
                      disabled={!isNew}
                      disableUnderline={!isNew}
                      classes={{
                        root: classes.white,
                        underline: classes.cssUnderline,
                        disabled: classes.white,
                      }}
                      value={ isSubmiited ? (o.text || '입력 없음') : (o.label || '')}
                      onChange={(e) => {
                        const { value } = e.target;
                        const prev = {
                          ...inputs[pageNum - 1][i],
                          label: value,
                        };
                        let ins = inputs;
                        ins[pageNum - 1][i] = value;
                        this.props.onInputChange(update(inputs, { [pageNum - 1]: { $splice: [[i, 1, prev]] } } ));
                      }}
                    />
                  </FormControl>
                </ListItem>
              );
            })
          }
          </List>
          <Divider/>
          {
            submitted ?
            <List >
              {
                submitted.map((o) => (
                  <ListItem
                    key={o.no}
                    onClick={() => handleClick('submit', o)}
                    button
                    className={classes.white}
                  >
                    <ListItemIcon className={classes.white}>
                      <Icon>person</Icon>
                    </ListItemIcon>
                    <ListItemText
                      classes={{
                        primary: classes.white,
                        secondary: classes.white,
                      }}
                      primary={o.title}
                      secondary={o.writer}
                    />
                  </ListItem>
                ))
              }
            </List> : null
          }
        </div>
      </Drawer>
    );
  }
}

export default withStyles(styles)(Sidebar);
