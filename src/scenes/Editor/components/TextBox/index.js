import React from 'react';
import {Editor} from 'draft-js';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

const styles = {
  labelWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.5,
    zIndex: 0,
  },
  // cannot know this classes in server
  textBox: {
    border: '1px solid gray',
    cursor: 'text',
  },
};
class TextBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      style: {
        editor: {
          position: 'absolute',
          zIndex: 10,
        },
      },
    };
    this.setEditor = (editor) => {
      this.editor = editor;
    };
    this.focusEditor = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };
  }
  componentDidMount() {
    if (this.props.focus)
      this.focusEditor();
  }
  // height 넘어서는 글자 못쓰도록 설정
  render() {
    const { classes, x, y, width, height, editorState, onChange, number, label, noBorder }  = this.props;
    const { editor } = this.state.style;
    return (
      <div
        className={noBorder ? null:classes.textBox}
        style={{ left: x, top: y, width, height, ...editor }}
        onClick={this.focusEditor}
      >
        <Editor
          ref={this.setEditor}
          editorState={editorState}
          onChange={onChange}
        />
        {
          label && !noBorder ?
          <div className={classes.labelWrapper}>
            <Typography variant="h6">
              {label}
            </Typography>
          </div> : null
        }
      </div>
    );
  }
}


export default withStyles(styles)(TextBox);
