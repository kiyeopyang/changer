import {
  actions,
  autoForm,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { response, error } = yield call(
    request,
    `/api/document/extractv2`,
    {
      method: 'POST',
      headers: {
        credentials: 'include',
      },
      body: params,
    },
  );
  if (response) {
    yield put(autoForm.success(params, response));
  }
  else yield put(autoForm.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
