import { combineReducers } from 'redux';
import autoForm from './autoForm/reducer';

export default combineReducers({
  autoForm,
});
