import React, { Component, Fragment } from 'react';
import pdfjsLib from 'pdfjs-dist';
import update from 'react-addons-update';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  withRouter,
} from 'react-router-dom';
import TextBox from './components/TextBox';
import { createHtml } from './modules';
import Layout from './components/Layout';
import IconButton from '@material-ui/core/IconButton';
import BeforeIcon from '@material-ui/icons/NavigateBefore';
import NextIcon from '@material-ui/icons/NavigateNext';
import Text from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Loader from '../../components/Loader';
import {
  EditorState,
  ContentState,
  convertToRaw,
  convertFromRaw,
} from 'draft-js';
import {
  autoForm,
} from './data/autoForm/actions';
import uuid from 'uuid/v1';
import Sidebar from './components/Sidebar';
import ImageBox from './components/ImageBox';
import SelectBox from './components/SelectBox';

const styles = {
  wrapperOfPageNums: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageNum: {
    fontSize: 36,
    display: 'inline-block',
    padding: 12,
    color: 'white',
  },
  submit: {
    padding: 32,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  downloadIcon: {
    fontSize: 36,
  },
  main: {
    // flex: 1,
    width: 'calc(100% - 260px)',
    background: 'rgb(34, 46, 62)',
    opacity: 0.95,
    paddingBottom: 80,
  },
  wrapper: { display: 'flex', justifyContent: 'center', alignItems: 'center' },
  editLayer: { border: '1px solid black', position: 'absolute' },
  pdf: { border: '1px solid black' },
  rightForms: {
    background: 'rgb(34, 46, 62)',
    minWidth: 300,
  },
};
class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum : 1,
      pdfDoc: null,
      pageRendering: false,
      pageNumPending: null,
      scale: 1.5,
      inputs: [],
      editLayerDownCoords: null,
      editLayerCurrentCoords: null,
      pdfSize: null,
      outerHtmls: [],

      loading: false,

      statusOfGettingOuterHtmls: 'nothing',
      pageNumForGettingOuterhtmls: 1,
      textBoxStyle: { border: '1px solid black', position: 'absolute' },

      createType: 'text',
      showAuto: false,
    };
    this.editCanvas = React.createRef();
    this.editLayer = React.createRef();
    this.canvas = React.createRef();
    this.fileSelection = React.createRef();
    this.uploadFileSelection = React.createRef();
  }
  componentDidUpdate() {
    const { statusOfGettingOuterHtmls, pageNumForGettingOuterhtmls, outerHtmls, pdfDoc} = this.state;
    if (statusOfGettingOuterHtmls === 'getting') {
      if (pageNumForGettingOuterhtmls < pdfDoc.numPages || !outerHtmls[pageNumForGettingOuterhtmls - 1]) {
        this.setState((state) => {
          const { outerHtmls } = state;

          if (outerHtmls[pageNumForGettingOuterhtmls - 1])
            return ({
              pageNumForGettingOuterhtmls: state.pageNumForGettingOuterhtmls + 1,
            });
          else {
            outerHtmls[pageNumForGettingOuterhtmls - 1] = this.editLayer.current.outerHTML;
            return ({
              outerHtmls,
            });
          }
        });
      } else {
        this.setState({
          statusOfGettingOuterHtmls: 'nothing',
          pageNumForGettingOuterhtmls: 1
        });
        this.convertToPdf();
      }
    } else
      this.drawEditCanvas();
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.pdfUrl !== nextProps.pdfUrl) {
      this.setState({ inputs: [], pdfDoc: null, outerHtmls: null, pageNum: 1, pdfSize: null });
      this.renderPdf({ url: nextProps.pdfUrl });
    }
    if (this.state.pdfDoc && this.props.submittedItem !== nextProps.submittedItem && !nextProps.submittedItemLoading) {
      if (nextProps.submittedItem) {
        const temps = nextProps.submittedItem.map((item, itemI) => item.map((form, formI) => {
          // 기존 폼 데이터 (좌표, 크기)
          const _form = this.props.forms[itemI][formI];
          console.log('_form', _form);
          return {
            label: _form.label,
            x: _form.x,
            y: _form.y,
            width: _form.width,
            height: _form.height,
            type: _form.type,
            value: form.value,
          }
        }));
        console.log(temps);
        this.setState({
          inputs: this.initForms(temps, this.state.pdfDoc.numPages),
        });
      } else {
        this.setState({
          inputs: this.initForms(this.props.forms, this.state.pdfDoc.numPages),
        });
      }
    }
  }
  componentDidMount() {
    const { pdfUrl } = this.props;
    if (pdfUrl) {
      this.renderPdf({ url: pdfUrl });
    }
  }
  getOuterHtmls = () => {
    this.setState({
      pdfNum: 1,
      outerHtmls: [],
    });
    setTimeout(() => {
      this.setState({
        statusOfGettingOuterHtmls: 'getting',
        pageNumForGettingOuterhtmls: 1,
      })
    }, 500)
  }
  initForms = (forms, numPages) => {
    let inputs = [];
    for (let i = 0; i< numPages; i += 1)
      inputs.push(forms[i] ? forms[i].map((o) => {
        const temp = {
          label: o.label,
          id: uuid(), // just key
          x: o.x,
          y: o.y,
          width: o.width,
          height: o.height,
          type: o.type,
        }
        if (o.type === 'text') {
          console.log(o);
          return {
            ...this.createTextBox(o.value ? JSON.parse(o.value):null),
            ...temp,
          };
        }
        else if (o.type === 'image') {
          return {
            ...temp,
            url: o.value ? `https://forjeeun.xyz${o.value}` : null,
          };
        }
      }) : []);
    return inputs;
  }
  renderDefault = () => {
    this.renderPdf({ url: 'b.pdf' });
  }
  onSelectFile = e => {
    const fileReader = new FileReader();
    fileReader.onloadend = e => {
      this.renderPdf({ data: e.target.result });
    }
    const file = e.target.files[0];
    fileReader.readAsArrayBuffer(file);
  }
  renderPdf = ({ url, data }) => {
    this.setState({ loading: true });
    pdfjsLib
      .getDocument(url ? url : { data })
      .then((pdfDocument) => {
        this.setState({
          pdfDoc: pdfDocument,
          outerHtmls: Array(pdfDocument.numPages).fill([]),
          inputs: this.initForms(this.props.forms, pdfDocument.numPages),
        });
        this.renderPage(1);
      })
      .catch((reason) => {
        this.setState({ loading: false });
        console.error('Error: ' + reason);
      });
  }
  renderPage(num) {
    this.setState({ pageRendering: true });
    this.state.pdfDoc.getPage(num).then((page) => {
      const PRINT_RESOLUTION = 300;
      const PRINT_UNITS = PRINT_RESOLUTION / 72.0;
      const { height, width, ...rest } = page.getViewport(this.state.scale);
      this.canvas.current.height = height;
      this.canvas.current.width = width;
      document.getElementById('canvas').onmousemove = this.onMouseMove;
      //
      if (this.props.isNew) {
        this.editCanvas.current.height = height;
        this.editCanvas.current.width = width;
      }
      //
  
      const renderContext = {
        transform: [PRINT_UNITS, 0, 0, PRINT_UNITS],
        canvasContext: this.canvas.current.getContext('2d'),
        viewport: { height, width, ...rest },
        intent: 'print',
      };
      const renderTask = page.render(renderContext);

      renderTask.promise.then(() => {
        // render success
        this.setState((state) => {
          const { inputs } = state;
          if (!inputs[num - 1]) {
            inputs[num - 1] = [];
          }
          return {
            pageRendering: false,
            pageNum: num,
            inputs,
            pdfSize: {
              height,
              width,
            },
            loading: false,
            showAuto: false,
          }
        });
        if (this.state.pageNumPending !== null) {
          this.setState({ pageNumPending: null });
        }
      });
    });
  }
  queueRenderPage = (num) => {
    this.setState((state) => {
      const { pageNum, outerHtmls } = state;
      outerHtmls[pageNum - 1] = this.editLayer.current.outerHTML;
      return {
        outerHtmls,
      };
    })
    if (this.state.pageRendering) {
      this.setState({ pageNumPending: num });
    } else {
      this.renderPage(num);
    }
  }
  onPrevPage = () => {
    if (this.state.pageNum <= 1) {
      return;
    }
    const pageNum = this.state.pageNum - 1;
    this.queueRenderPage(pageNum);
  }
  onNextPage = () => {
    if (this.state.pageNum >= this.state.pdfDoc.numPages) {
      return;
    }
    const pageNum = this.state.pageNum + 1;
    this.queueRenderPage(pageNum);
  }
  createTextBox = (value) => {
    const box = {
      editorState: EditorState.createEmpty(),
    };
    if (value) {
      const editorState = EditorState.createWithContent(convertFromRaw(value));
      box.editorState = editorState;
      box.text = editorState.getCurrentContent().getPlainText() || '' ;
    }
    return box;
  };
  handleEditCanvasEvents = name => (e) => {
    const { createType } = this.state;
    if (this.state.loading) return e;
    e.preventDefault();
    e.stopPropagation();
    const { x, y } = this.editCanvas.current.getBoundingClientRect();
    const relativeX = Math.round(e.clientX - x);
    const relativeY = Math.round(e.clientY - y);
    switch (name) {
      case 'down': {
        if (!this.isThisCoordsInvalid({ x: relativeX, y: relativeY })) {
          const editLayerDownCoords = { x: relativeX, y: relativeY };
          this.setState({
            editLayerDownCoords,
          });
        }
        break;
      }
      case 'up': {
        if (this.state.editLayerDownCoords && this.state.editLayerCurrentCoords) {
          this.clearEditCanvas();
          let { 
            editLayerDownCoords,
            editLayerCurrentCoords,
            inputs,
            pageNum,
          } = this.state;
          const isXreversed = editLayerDownCoords.x > editLayerCurrentCoords.x;
          const isYreversed = editLayerDownCoords.y > editLayerCurrentCoords.y;
          const posAndSize = {
            x: isXreversed ? editLayerCurrentCoords.x : editLayerDownCoords.x,
            y: isYreversed ? editLayerCurrentCoords.y : editLayerDownCoords.y,
            width: (editLayerCurrentCoords.x - editLayerDownCoords.x) * (isXreversed ? -1 : 1),
            height: (editLayerCurrentCoords.y - editLayerDownCoords.y) * (isYreversed ? -1 : 1),
          };
          if (createType === 'text') {
            const textBox = {
              ...posAndSize,
              type: 'text',
              ...this.createTextBox(),
              id: uuid(),
            };
            if (this.checkIsValidPos({
              x: textBox.x,
              y: textBox.y,
              width: textBox.width,
              height: textBox.height,
            }) && textBox.width > 50 && textBox.height > 15) {
              inputs[pageNum - 1] = inputs[pageNum - 1].concat([textBox]);
            }
          } else {
            const imageBox = {
              ...posAndSize,
              type: 'image',
              id: uuid(),
            };
            if (this.checkIsValidPos({
              x: imageBox.x,
              y: imageBox.y,
              width: imageBox.width,
              height: imageBox.height,
            }) && imageBox.width > 30 && imageBox.height > 30) {
              inputs[pageNum - 1] = inputs[pageNum - 1].concat([imageBox]);
            }
          }
          this.setState({
            editLayerDownCoords: null,
            editLayerCurrentCoords: null,
            inputs,
          });
        }
        break;
      }
      case 'move': {
        if (this.state.editLayerDownCoords) {
          this.setState({
            editLayerCurrentCoords: { x: relativeX, y: relativeY },
          });
        }
        break;
      }
      default:
    }
  }
  checkIsValidPos = (toBeMade = { x: 0, y: 0, width: 0, height: 0 }) => {
    const { inputs, pageNum } = this.state;
    let isValid = true;
    inputs[pageNum - 1].forEach((input) => {
      // 필드 안에 만들 필드의 좌표가 포함 되어있나
      const { x, y, width, height } = toBeMade;
      const pos = [{ x, y }, { x: x + width, y }, { x, y: y + height }, { x: x + width, y: y + height }];
      if (pos.some(({ x, y }) => this.isThisCoordsInRect({ x, y }, { x: input.x, y: input.y, width: input.width, height: input.height }))) {
        isValid = false;
      }
      // 만들 필드의 안에 필드의 좌표들이 포함되어 있나
      const inputPos = [{ x: input.x, y: input.y }, { x: input.x + input.width, y: input.y }, { x: input.x, y: input.y + input.height }, { x: input.x + input.width, y: input.y + input.height }];
      if (inputPos.some(({ x, y }) => this.isThisCoordsInRect({ x, y }, { x: toBeMade.x, y: toBeMade.y, width: toBeMade.width, height: toBeMade.height }))) {
        isValid = false;
      }
    })
    return isValid;
  }
  isThisCoordsInvalid = (coords) => {
    const { inputs, pageNum } = this.state;
    if (inputs[pageNum - 1].length && inputs[pageNum - 1].some((input) => this.isThisCoordsInRect(coords,input))) {
        return true;
    }
    return false;
  }
  isThisCoordsInRect = (coords = { x: 0, y: 0 }, rect = { x: 0, y: 0, width: 0, height: 0 }) => {
    if (coords.x >= rect.x && coords.y >= rect.y && coords.x <= rect.x + rect.width && coords.y <= rect.y + rect.height) {
      return true;
    }
    return false;
  };
  clearEditCanvas = () => {
    const canvas = this.editCanvas.current;
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  drawEditCanvas = () => {
    const {
      editLayerDownCoords,
      editLayerCurrentCoords,
    } = this.state;
    if (editLayerDownCoords && editLayerCurrentCoords) {
      this.clearEditCanvas();
      const canvas = this.editCanvas.current;
      const ctx = canvas.getContext('2d');
      ctx.strokeRect(
        editLayerDownCoords.x,
        editLayerDownCoords.y,
        editLayerCurrentCoords.x - editLayerDownCoords.x, // width
        editLayerCurrentCoords.y - editLayerDownCoords.y, // height
      );
    };
  }
  outputForms = () => {
    this.props.handleClick('save', this.state.inputs.map(j => j.map((o, i) => ({
      x: o.x,
      y: o.y,
      width: o.width,
      height: o.height,
      type: o.type,//'text',
      label: o.label,
    }))));
  };
  convertInputsToRaw = () => {
    const { inputs, pageNum } = this.state;
    const raws = inputs[pageNum - 1].map((input, i) => {
      return JSON.stringify(convertToRaw(input.editorState.getCurrentContent()));
    });
    return raws;
  }
  convertToPdf = () => {
    const outerHtmls = this.state.outerHtmls;
    const input = {
      htmls: outerHtmls.map(createHtml),
    };
    console.log(input);
    this.props.handleClick('pdf', input);
  }
  onImageUpload = i => (file) => {
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const dataUrl = e.target.result;
        this.setState(state => {
          const { inputs, pageNum } = state;
          const input = inputs[pageNum - 1][i];
          input.url = dataUrl;
          return { inputs: update(inputs, { [pageNum - 1]: { $splice: [[i, 1, input]] } } ) };
        });
      }
      reader.readAsDataURL(file);
    }
  }
  requestAutoForm = () => {
    const canvas = this.canvas.current;
    console.log('on')
    const ctx = canvas.getContext('2d');
    this.canvas.current.toBlob((blob) => {
      const formData = new FormData();
      console.log(blob);
      formData.set('file', blob, `${new Date().getTime()}.jpg`);
      this.props.requestAutoForm(formData);
    }, 'image/jpeg', 1);
  }
  isSameColor = (color1, color2) => {
    if (Array.isArray(color1) && Array.isArray(color2) &&
      color1[0] === color2[0] &&
      color1[1] === color2[1] &&
      color1[2] === color2[2]
    ) {
      return true;
    } else {
      return false;
    }
  }
  onMouseMove = (e) => {
    const canvas = this.canvas.current;
    const ctx = canvas.getContext('2d');
    var mouseX, mouseY;
    if(e.offsetX) {
      mouseX = e.offsetX;
      mouseY = e.offsetY;
    }
    else if(e.layerX) {
      mouseX = e.layerX;
      mouseY = e.layerY;
    }
  }
  handleSidebarClick = (label, v) => {
    switch (label) {
      case 'pdf': 
        this.getOuterHtmls();
        break;
      case 'save':
        this.outputForms();
        break;
      case 'link':
        this.props.handleClick('copy');
        break;
      case 'showAuto': {
        const canvas = this.canvas.current;
        const ctx = canvas.getContext('2d');
        if (!this.state.showAuto) {
          this.requestAutoForm();
        } else {

        }
        this.setState({
          showAuto: !this.state.showAuto,
        });
        break;
      }
      default:
        this.props.handleClick(label, v);
        break;
    }
  }
  render() {
    const { classes, autoForm, inputFromForm, isNew, submittedItem, handleClick, submitted, submittedItemLoading, ...rest } = this.props;
    const { showAuto, createType, pdfSize, inputs, pageNum, loading, statusOfGettingOuterHtmls, pageNumForGettingOuterhtmls } = this.state;
    const autoForms = autoForm.response && autoForm.response.map(o => o.reduce((acc, val, i) => {
      const target = i === 0 ? 'x' : i === 1 ? 'y' : i === 2 ? 'width' : i === 3 ? 'height' : null;
      if (target) acc[target] = val;
      return acc;
    }, {}));
    console.log(autoForms);
    return (
      <Layout>
        <div className={classes.main}>
          <div className={classes.wrapperOfPageNums}>
            <IconButton onClick={this.onPrevPage}>
              <BeforeIcon className={classes.pageNum}/>
            </IconButton>
            <Text className={classes.pageNum}>
              {this.state.pageNum}
            </Text>
            <IconButton onClick={this.onNextPage}>
              <NextIcon className={classes.pageNum}/>
            </IconButton>
          </div>
          <div className={classes.wrapper}>
            {
              pdfSize ?
                <Fragment>
                  <div
                    className={classes.editLayer}
                    ref={this.editLayer}
                    style={{ width: pdfSize.width, height: pdfSize.height }}
                  >
                  {
                    statusOfGettingOuterHtmls === 'getting' ?
                      inputs[pageNumForGettingOuterhtmls - 1].map((input, i) => {
                        if (input.type === 'text') {
                          return (
                            <TextBox
                              key={input.id}
                              editorState={input.editorState}
                              x={input.x}
                              y={input.y}
                              width={input.width}
                              height={input.height}
                              onChange={() => {}}
                            />
                          );
                      } else if (input.type === 'image') {
                        return (
                          <ImageBox
                            key={input.id}
                            url={input.url}
                            x={input.x}
                            y={input.y}
                            width={input.width}
                            height={input.height}
                            readOnly
                          />
                        );
                      }
                    }):
                    inputs[pageNum - 1].map((input, i) => {
                      if (input.type === 'text') {
                        return (
                          <TextBox
                            key={input.id}
                            onChange={editorState => this.setState((state) => {
                              input.editorState = editorState;
                              return { inputs: update(state.inputs, { [pageNum - 1]: { $splice: [[i, 1, input]] } } ) };
                            })}
                            editorState={input.editorState}
                            x={input.x}
                            y={input.y}
                            width={input.width}
                            height={input.height}
                            label={input.label}
                            number={i+1}
                            noBorder={submittedItem || submittedItemLoading}
                          />
                        );
                    } else if (input.type === 'image') {
                      return (
                        <ImageBox
                          key={input.id}
                          onChange={this.onImageUpload(i)}
                          url={input.url}
                          x={input.x}
                          y={input.y}
                          width={input.width}
                          height={input.height}
                          noBorder={submittedItem || submittedItemLoading}
                        />
                      );
                    }
                  })
                  }
                </div>
              </Fragment>
              : null
            }
            {
              isNew ?
              <canvas
                className={classes.editLayer}
                onMouseDown={this.handleEditCanvasEvents('down')}
                onMouseUp={this.handleEditCanvasEvents('up')}
                onMouseMove={this.handleEditCanvasEvents('move')}
                ref={this.editCanvas}
                id="editCanvas"
              /> : null
            }
            <canvas className={classes.pdf} ref={this.canvas} id="canvas" />
          </div>
        </div>
        <Sidebar
          showAuto={showAuto}
          handleClick={this.handleSidebarClick}
          inputs={inputs}
          pageNum={pageNum}
          isNew={isNew}
          onInputChange={(inputs) => this.setState({ inputs })}
          submitted={submitted}
          createType={createType}
          onChange={createType => this.setState({ createType })}
        />
        {
          loading || autoForm.loading ? <Loader /> : null
        }
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  autoForm: state.Editor.data.autoForm,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  requestAutoForm: autoForm.request,
}, dispatch);
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(Editor)));
