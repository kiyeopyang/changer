export default ele => `
  <html>
    <head>
      <style>
        html, body {
          margin: 0;
          padding: 0;
          font-family: sans-serif;
        }
      </style>
    </head>
    <body>
      ${ele}
    </body>
  </html>
`;
