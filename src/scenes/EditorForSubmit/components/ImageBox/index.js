import React, { Fragment } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import IconButton from '@material-ui/core/IconButton';
import PhotoIcon from '@material-ui/icons/Photo';

const styles = {
  labelWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 0,
  },
  // cannot know this classes in server
  imageBox: {
    border: '1px solid gray',
    cursor: 'pointer',
  },
  input: {
    display: 'none',
  },
};
const imgStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  bottom: 0,
  right: 0,
  width: '100%',
};
class ImageBox extends React.Component {
  // height 넘어서는 글자 못쓰도록 설정
  render() {
    const { classes, x, y, width, height, onChange, url, noBorder, readOnly }  = this.props;
    if (readOnly) {
      return (
        <div
          className={noBorder ? null : classes.imageBox}
          style={{
            position: 'absolute',
            zIndex: 10,
            left: x,
            top: y,
            width,
            height,
            paddingTop: `${height/width}%`,
            overflow: 'hidden',
          }}
        >
          <img
            style={imgStyle}
            src={url}
            alt={url}
          />
        </div>
      )
    }
    return (
      <Fragment>
        <input
          accept="image/*"
          className={classes.input}
          id={`${x}${y}${width}${height}`}
          type="file"
          onChange={(e) => e.target.files ? onChange(e.target.files[0]):null}
        />
        <label htmlFor={`${x}${y}${width}${height}`}>
          <div
            className={noBorder ? null : classes.imageBox}
            style={{
              position: 'absolute',
              zIndex: 10,
              left: x,
              top: y,
              width,
              height,
              paddingTop: `${height/width}%`,
              overflow: 'hidden',
            }}
            role="button"
          >
          {
            url ?
            <img
              style={imgStyle}
              src={url}
              alt={url}
            /> :
            <div className={classes.labelWrapper}>
              <IconButton variant="contained" component="span">
                <PhotoIcon />
              </IconButton>
            </div>
          } 
          </div>
        </label>
      </Fragment>
    );
  }
}


export default withStyles(styles)(ImageBox);
