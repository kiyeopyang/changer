import React, { Fragment } from "react";
// javascript plugin used to create scrollbars on windows
import classnames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
import Divider from '@material-ui/core/Divider';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import PhotoIcon from '@material-ui/icons/Photo';
import Text from '@material-ui/core/Typography';
import Loader from '../../../../components/Loader';

// core components

const styles = theme => ({
  list: {
    width: 260,
    background: 'rgb(34, 46, 62)',
    position: 'static',
    boxShadow: '0px 8px 10px 5px rgba(0, 0, 0, 0.42), 0px 16px 24px 2px rgba(0, 0, 0, 0.12), 0px 6px 30px 5px rgba(0, 0, 0, 0.2)',
  },
  mobile: {
    width: '100%',
    position: 'fixed',
  },
  white: {
    color: 'white',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  cssLabel: {
    color: 'white',
    '&$cssFocused': {
      color: '#e91e63',
    },
  },
  cssFocused: {
    color: 'white',
    borderBottomColor: 'white',
  },
  cssUnderline: {
    color: 'white',
    borderBottomColor: 'white',
    '&:after': {
      color: 'white',
      borderBottomColor: '#e91e63',
    },
    '&:before': {
      color: 'white',
      borderBottomColor: 'white',
    }
  },
  mobileLabel: {
    fontSize: 30,
  },
  mobileIcon: {
    fontSize: 64,
  },
  mobileTextInput: {
    fontSize: 100,
  },
  input: {
    display: 'none',
  },
});
class Sidebar extends React.Component {
  render() {
    const {
      classes,
      handleClick,
      inputs,
      onlySubmit,
      values,
      handleChange,
      disableSubmit,
      isMobile,
      open = true,
      onClose,
      onTextChange,
      onImageChange,
      loading,
    } = this.props;
    console.log(inputs);
    console.log(this.props);
    return (
      <Drawer
        variant={isMobile ? undefined : "permanent"}
        anchor={isMobile ? "bottom":"right"}
        open={open}
        onClose={onClose}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
        classes={{
          paper: classnames(classes.list, { [classes.mobile]: isMobile }),
        }}
      >
        <div ref="sidebarWrapper">
          <List >
            {
              isMobile ?
              <ListItem onClick={onClose} button className={classes.white}>
                <ListItemIcon className={classes.white}>
                  <Icon className={classnames({ [classes.mobileIcon]: isMobile })} >close</Icon>
                </ListItemIcon>
                <ListItemText
                  primary={"닫기"}
                  disableTypography={true}
                  style={isMobile ? {fontSize: 48}:{}}
                />
              </ListItem> : null
            }
            {
              onlySubmit ?
              <ListItem disabled={disableSubmit} onClick={() => handleClick('submit')} button className={classes.white}>
                <ListItemIcon className={classes.white}>
                  <Icon className={classnames({ [classes.mobileIcon]: isMobile })} >save</Icon>
                </ListItemIcon>
                <ListItemText
                  primary={"제출하기"}
                  disableTypography={true}
                  style={isMobile ? {fontSize: 48}:{}}
                />
              </ListItem> : null
            }
           
          </List>
          <Divider style={{background:'white', opacity: 0.1}}/>
          <List>
            <ListItem>
              <FormControl fullWidth>
                <InputLabel
                  htmlFor={`title`}
                  classes={{
                    root: classnames(classnames(classes.cssLabel, { [classes.mobileLabel]: isMobile }), { [classes.mobileLabel]: isMobile }),
                    focused: classes.cssFocused,
                    disabled: classnames(classnames(classes.cssLabel, { [classes.mobileLabel]: isMobile }), { [classes.mobileLabel]: isMobile }),
                  }}
                >
                  {"타이틀"}
                </InputLabel>
                <Input
                  fullWidth
                  id={`title`}
                  classes={{
                    root: classnames(classes.white, { [classes.mobileTextInput]: isMobile }),
                    underline: classes.cssUnderline,
                    disabled: classnames(classes.white, { [classes.mobileTextInput]: isMobile }),
                  }}
                  value={values.title}
                  onChange={handleChange('title')}
                />
              </FormControl>
            </ListItem>
            <ListItem >
              <FormControl fullWidth>
                <InputLabel
                  htmlFor={'writer'}
                  classes={{
                    root: classnames(classes.cssLabel, { [classes.mobileLabel]: isMobile }),
                    focused: classes.cssFocused,
                    disabled: classnames(classes.cssLabel, { [classes.mobileLabel]: isMobile }),
                  }}
                >
                  {`작성자`}
                </InputLabel>
                <Input
                  fullWidth
                  id={'writer'}
                  classes={{
                    root: classnames(classes.white, { [classes.mobileTextInput]: isMobile }),
                    underline: classes.cssUnderline,
                    disabled: classnames(classes.white, { [classes.mobileTextInput]: isMobile }),
                  }}
                  value={values.writer}
                  onChange={handleChange('writer')}
                />
              </FormControl>
            </ListItem>
          <Divider style={{background:'white', opacity: 0.1}}/>
          {
            inputs && inputs.map((o, i) => (
              <ListItem key={`page, ${i}${o.label}`}>
                <FormControl fullWidth>
                  {
                    o.type === 'text' ?
                    <Fragment>
                      <InputLabel
                        htmlFor={`page, ${i}${o.label}`}
                        classes={{
                          root: classnames(classes.cssLabel, { [classes.mobileLabel]: isMobile }),
                          focused: classes.cssFocused,
                          disabled: classnames(classes.cssLabel, { [classes.mobileLabel]: isMobile }),
                        }}
                      >
                        {o.label}
                      </InputLabel>
                      <Input
                        fullWidth
                        id={`page, ${i}${o.label}`}
                        classes={{
                          root: classnames(classes.white, { [classes.mobileTextInput]: isMobile }),
                          underline: classes.cssUnderline,
                          disabled: classnames(classes.white, { [classes.mobileTextInput]: isMobile }),
                        }}
                        multiline
                        value={o.textForForm || ''}
                        onChange={(e) => {
                          const { value } = e.target;
                          this.props.onTextChange(i, value);
                        }}
                      />
                    </Fragment>:
                    <Fragment>
                      <input
                        accept="image/*"
                        className={classes.input}
                        id={`page, ${i}${o.label}`}
                        type="file"
                        onChange={(e) => e.target.files ? onImageChange(i, e.target.files[0]):null}
                      />
                      <label htmlFor={`page, ${i}${o.label}`}>
                        <div role="button">
                        <Text className={classnames(classes.cssLabel, { [classes.mobileLabel]: isMobile })}>{o.label}</Text>
                        {
                          o.url ?
                          <div style={{padding: '10%'}}>
                            <img
                              style={{width: '100%'}}
                              src={o.url}
                              alt={o.url}
                            />
                          </div>:
                          <div>
                            <IconButton variant="contained" component="span">
                              <PhotoIcon className={classnames(classes.white, { [classes.mobileIcon]: isMobile })} />
                            </IconButton>
                          </div>
                        } 
                        </div>
                      </label>
                    </Fragment>
                  }
                </FormControl>
              </ListItem>
            ))
          }
          </List>
        </div>
        {
          loading && <Loader />
        }
      </Drawer>
    );
  }
}

export default withStyles(styles)(Sidebar);
