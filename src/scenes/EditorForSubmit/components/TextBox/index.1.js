import React from 'react';
import {Editor} from 'draft-js';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

const styles = {
  numberWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    display: 'flex',
    flexDirection: 'alignItems',
    justifyContent: 'center',
    opacity: 0.7,
  },
};
class TextBox extends React.Component {
  constructor(props) {
    super(props);
    this.setEditor = (editor) => {
      this.editor = editor;
    };
    this.focusEditor = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };
  }
  componentDidMount() {
    if (this.props.focus)
      this.focusEditor();
  }
  // height 넘어서는 글자 못쓰도록 설정
  render() {
    const { classes, x, y, width, height, editorState, onChange, number }  = this.props;
    return (
      <div style={{zIndex: 10, left: x, top: y, width, height, ...styles.editor}} onClick={this.focusEditor}>
        <Editor
          ref={this.setEditor}
          editorState={editorState}
          onChange={onChange}
        />
        {/* {
          number ?
          <div className={classes.numberWrapper}>
            <Typography variant="h4">
              {number}
            </Typography>
          </div> : null
        } */}
      </div>
    );
  }
}

export default withStyles(styles)(TextBox);
