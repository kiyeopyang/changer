import React from 'react';
import {Editor} from 'draft-js';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

const styles = {
  numberWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.5,
    zIndex: 0,
  },
  editor: {
    position: 'absolute',
    border: '1px solid gray',
    cursor: 'text',
    zIndex: 10,
  },
};
class TextBox extends React.Component {
  constructor(props) {
    super(props);
    this.setEditor = (editor) => {
      this.editor = editor;
    };
    this.focusEditor = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };
  }
  componentDidMount() {
    if (this.props.focus)
      this.focusEditor();
  }
  // height 넘어서는 글자 못쓰도록 설정
  render() {
    const { classes, x, y, width, height, editorState, onChange, number, label, readOnly }  = this.props;
    return (
      <div className={classes.editor} style={{ left: x, top: y, width, height }} onClick={this.focusEditor}>
        <Editor
          readOnly={readOnly}
          ref={this.setEditor}
          editorState={editorState}
          onChange={onChange}
        />
        {
          label ?
          <div className={classes.numberWrapper}>
            <Typography variant="h6">
              {label}
            </Typography>
          </div> : null
        }
      </div>
    );
  }
}


export default withStyles(styles)(TextBox);
