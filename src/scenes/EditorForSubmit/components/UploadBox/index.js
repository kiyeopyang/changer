import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import IconButton from '@material-ui/core/IconButton';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Layout from '../Layout';

const styles = ({
  button: {
    fontSize: 150,
  },
  icon: {
    fontSize: 150,
  },
  input: {
    display: 'none',
  },
});
const UploadBox = ({ classes, onUpload }) => (
  <Layout>
    <input
      id="pdfUploadButton"
      className={classes.input}
      onChange={onUpload}
      type="file"
    />
    <label htmlFor="pdfUploadButton">
      <IconButton color="primary" className={classes.button} variant="contained" component="span">
        <CloudUploadIcon className={classes.icon}/>
      </IconButton>
    </label>
  </Layout>
);
export default withStyles(styles)(UploadBox);
