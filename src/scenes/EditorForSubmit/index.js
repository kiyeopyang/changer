import React, { Component, Fragment } from 'react';
import pdfjsLib from 'pdfjs-dist';
import update from 'react-addons-update';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  withRouter,
} from 'react-router-dom';
import TextBox from './components/TextBox';
import { createHtml } from './modules';
import Layout from './components/Layout';
import IconButton from '@material-ui/core/IconButton';
import BeforeIcon from '@material-ui/icons/NavigateBefore';
import NextIcon from '@material-ui/icons/NavigateNext';
import MenuIcon from '@material-ui/icons/Menu';
import Text from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import compress from 'browser-image-compression';
import exif from 'exif-js';
import {
  EditorState,
  ContentState,
  convertToRaw,
  convertFromRaw,
} from 'draft-js';
import uuid from 'uuid/v1';
import Sidebar from './components/Sidebar';
import ImageBox from './components/ImageBox';
import Loader from '../../components/Loader';

const styles = {
  wrapperOfPageNums: {
    background: 'rgb(34, 46, 62)',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageNum: {
    fontSize: 36,
    display: 'inline-block',
    padding: 12,
    color: 'white',
  },
  submit: {
    padding: 32,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  downloadIcon: {
    fontSize: 36,
  },
  main: {
    flex: 1,
    background: 'rgb(34, 46, 62)',
    opacity: 0.95,
    paddingBottom: 80,
    minWidth: 1000,
  },
  wrapper: { display: 'flex', justifyContent: 'center', alignItems: 'center' },
  editLayer: { border: '1px solid black', position: 'absolute' },
  pdf: {
    border: '1px solid black',
  },
  rightForms: {
    background: 'rgb(34, 46, 62)',
    minWidth: 300,
  },
  menuIcon: {
    color: 'white',
    fontSize: 36,
  },
};
class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum : 1,
      pdfDoc: null,
      pageRendering: false,
      pageNumPending: null,
      scale: 1.5,
      inputs: [],
      editLayerDownCoords: null,
      editLayerCurrentCoords: null,
      pdfSize: null,
      outerHtmls: [],

      loading: false,

      statusOfGettingOuterHtmls: 'nothing',
      pageNumForGettingOuterhtmls: 1,

      title: '',
      writer: '',

      isMobile: false,
      drawerOpen: false,
      
      createType: 'text',
    };
    this.editCanvas = React.createRef();
    this.editLayer = React.createRef();
    this.canvas = React.createRef();
    this.fileSelection = React.createRef();
    this.uploadFileSelection = React.createRef();
  }
  componentDidUpdate() {
    const { statusOfGettingOuterHtmls, pageNumForGettingOuterhtmls, outerHtmls, pdfDoc} = this.state;
    if (statusOfGettingOuterHtmls === 'getting') {
      if (pageNumForGettingOuterhtmls < pdfDoc.numPages || !outerHtmls[pageNumForGettingOuterhtmls - 1]) {
        this.setState((state) => {
          const { outerHtmls } = state;

          if (outerHtmls[pageNumForGettingOuterhtmls - 1])
            return ({
              pageNumForGettingOuterhtmls: state.pageNumForGettingOuterhtmls + 1,
            });
          else {
            outerHtmls[pageNumForGettingOuterhtmls - 1] = this.editLayer.current.outerHTML;
            return ({
              outerHtmls,
            });
          }
        });
      } else {
        this.setState({
          statusOfGettingOuterHtmls: 'nothing',
          pageNumForGettingOuterhtmls: 1
        });
        this.convertToPdf();
      }
    } else
      this.drawEditCanvas();
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.pdfUrl !== nextProps.pdfUrl) {
      this.setState({ inputs: [], pdfDoc: null, outerHtmls: null, pageNum: 1, pdfSize: null });
      this.renderPdf({ url: nextProps.pdfUrl });
    }
  }
  componentDidMount() {
    const { pdfUrl, forms } = this.props;
    if (pdfUrl) {
      this.renderPdf({ url: pdfUrl });
    }
    console.log(window.screen.width);
    this.setState({
      isMobile: window.screen.width <= 600,
    });
  }
  getOuterHtmls = () => {
    this.setState({
      pdfNum: 1,
      outerHtmls: [],
    });
    setTimeout(() => {
      this.setState({
        statusOfGettingOuterHtmls: 'getting',
        pageNumForGettingOuterhtmls: 1,
      })
    }, 500)
  }
  // initForms = (forms, numPages) => {
  //   let inputs = [];
  //   for (let i = 0; i< numPages; i += 1)
  //     inputs.push(forms[i] ? forms[i].map((o) => this.createTextBox(o)) : []);
  //   return inputs;
  // }
  initForms = (forms, numPages) => {
    let inputs = [];
    for (let i = 0; i< numPages; i += 1)
      inputs.push(forms[i] ? forms[i].map((o) => {
        const temp = {
          label: o.label,
          id: uuid(), // just key
          x: o.x,
          y: o.y,
          width: o.width,
          height: o.height,
          type: o.type,
        }
        if (o.type === 'text') {
          return {
            ...this.createTextBox(o.value),
            ...temp,
          };
        }
        else {
          return {
            ...temp,
            url: o.value,
          };
        }
      }) : []);
    return inputs;
  }
  renderDefault = () => {
    this.renderPdf({ url: 'b.pdf' });
  }
  onSelectFile = e => {
    const fileReader = new FileReader();
    fileReader.onloadend = e => {
      this.renderPdf({ data: e.target.result });
    }
    const file = e.target.files[0];
    fileReader.readAsArrayBuffer(file);
  }
  renderPdf = ({ url, data }) => {
    pdfjsLib
      .getDocument(url ? url : { data })
      .then((pdfDocument) => {
        this.setState({
          pdfDoc: pdfDocument,
          outerHtmls: Array(pdfDocument.numPages).fill([]),
          inputs: this.initForms(this.props.forms, pdfDocument.numPages),
        });
        this.renderPage(1);
      })
      .catch((reason) => {
        console.error('Error: ' + reason);
      });
  }
  renderPage(num) {
    this.setState({ pageRendering: true });
    this.state.pdfDoc.getPage(num).then((page) => {
      const { height, width, ...rest } = page.getViewport(this.state.scale);
      this.canvas.current.height = height;
      this.canvas.current.width = width;

      //
      if (this.props.isNew) {
        this.editCanvas.current.height = height;
        this.editCanvas.current.width = width;
      }
      //
  
      const renderContext = {
        canvasContext: this.canvas.current.getContext('2d'),
        viewport: { height, width, ...rest },
      };
      const renderTask = page.render(renderContext);
  
      renderTask.promise.then(() => {
        // render success
        this.setState((state) => {
          const { inputs } = state;
          if (!inputs[num - 1]) {
            inputs[num - 1] = [];
          }
          return {
            pageRendering: false,
            pageNum: num,
            inputs,
            pdfSize: {
              height,
              width,
            },
          }
        });
        if (this.state.pageNumPending !== null) {
          this.renderPage(this.state.pageNumPending);
          this.setState({ pageNumPending: null });
        }
      });
    });
  }
  queueRenderPage = (num) => {
    this.setState((state) => {
      const { pageNum, outerHtmls } = this.state;
      outerHtmls[pageNum - 1] = this.editLayer.current.outerHTML;
      return {
        outerHtmls,
      };
    })
    if (this.state.pageRendering) {
      this.setState({ pageNumPending: num });
    } else {
      this.renderPage(num);
    }
  }
  onPrevPage = () => {
    if (this.state.pageNum <= 1) {
      return;
    }
    const pageNum = this.state.pageNum - 1;
    this.queueRenderPage(pageNum);
  }
  onNextPage = () => {
    if (this.state.pageNum >= this.state.pdfDoc.numPages) {
      return;
    }
    const pageNum = this.state.pageNum + 1;
    this.queueRenderPage(pageNum);
  }
  createTextBox = () => {
    const box = {
      editorState: EditorState.createEmpty(),
    };
    return box;
  };
  clearEditCanvas = () => {
    const canvas = this.editCanvas.current;
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  drawEditCanvas = () => {
    const {
      editLayerDownCoords,
      editLayerCurrentCoords,
    } = this.state;
    if (editLayerDownCoords && editLayerCurrentCoords) {
      this.clearEditCanvas();
      const canvas = this.editCanvas.current;
      const ctx = canvas.getContext('2d');
      ctx.strokeRect(
        editLayerDownCoords.x,
        editLayerDownCoords.y,
        editLayerCurrentCoords.x - editLayerDownCoords.x, // width
        editLayerCurrentCoords.y - editLayerDownCoords.y, // height
      );
    };
  }
  convertInputsToRaw = () => {
    const { inputs } = this.state;
    const values = inputs.map(o => o.map((input, i) => {
      if (input.type === 'text') {
        return { value: JSON.stringify(convertToRaw(input.editorState.getCurrentContent())) };
      } else if (input.type === 'image') {
        return { dataUri: input.url };
      }
    }));
    return values;
  }
  handleSidebarClick = (label, v) => {
    switch (label) {
      case 'submit':
        const title = this.state.title;
        const writer = this.state.writer;
        const forms = this.convertInputsToRaw();
        this.props.handleClick('submit', {
          title,
          writer,
          forms,
        })
        break;
      default:
    }
  }
  getExif = async file => new Promise(resolve => exif.getData(file, () => resolve(exif.getTag(file, "Orientation"))));
  resetOrientation = async (srcBase64, srcOrientation) => new Promise((resolve) => {
    var img = new Image();    
  
    img.onload = function() {
      var width = img.width,
          height = img.height,
          canvas = document.createElement('canvas'),
          ctx = canvas.getContext("2d");
  
      // set proper canvas dimensions before transform & export
      if (4 < srcOrientation && srcOrientation < 9) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }
  
      // transform context before drawing image
      switch (srcOrientation) {
        case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
        case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
        case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
        case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
        case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
        case 7: ctx.transform(0, -1, -1, 0, height, width); break;
        case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
        default: break;
      }
  
      // draw image
      ctx.drawImage(img, 0, 0);
      canvas.toBlob((blob) => {
          resolve({ blob, dataUrl: canvas.toDataURL() });
        }, 'image/jpeg', 0.95);
    };
    
      img.src = srcBase64;
    }
  )
  onImageUpload = i => (file) => {
    if (file) {
      const reader = new FileReader();
      reader.onload = async (e) => {
        this.setState({ loading: true });
        const ori = await this.getExif(file);
        const { blob } = await this.resetOrientation(e.target.result, ori);
        const dataUrl = await compress.getDataUrlFromFile(await compress(blob, 0.5, 1920));

        this.setState(state => {
          const { inputs, pageNum } = state;
          const input = inputs[pageNum - 1][i];
          input.url = dataUrl;
          return {
            inputs: update(inputs, { [pageNum - 1]: { $splice: [[i, 1, input]] } } ),
            loading: false,
          };
        });
      }
      reader.readAsDataURL(file);
    }
  }
  render() {
    const { classes, inputFromForm, isNew, onlySubmit } = this.props;
    const { pdfSize, inputs, pageNum, loading, isMobile, statusOfGettingOuterHtmls, pageNumForGettingOuterhtmls, drawerOpen } = this.state;
    return (
      <Layout>
        <div className={classes.main}>
          <div
            className={classes.wrapperOfPageNums}
            style={isMobile ? {
              position: 'fixed',
              width: '100%',
            }: {}}
          >
            <IconButton onClick={this.onPrevPage}>
              <BeforeIcon className={classes.pageNum} style={isMobile ? {fontSize:70}:{}}/>
            </IconButton>
            <Text className={classes.pageNum} style={isMobile ? {fontSize:70}:{}}>
              {this.state.pageNum}
            </Text>
            <IconButton onClick={this.onNextPage}>
              <NextIcon className={classes.pageNum}style={isMobile ? {fontSize:70}:{}}/>
            </IconButton>
            {
              isMobile &&
              <IconButton style={isMobile ? { position: 'absolute', right: 30 }:{}} onClick={() => this.setState({ drawerOpen: true })}>
                <MenuIcon className={classes.pageNum} style={isMobile ? {fontSize:70}:{}}/>
              </IconButton>
            }
          </div>
          <div className={classes.wrapper} style={{marginTop: isMobile ? 140:0}}>
            {
              pdfSize ?
                <Fragment>
                  <div
                    className={classes.editLayer}
                    ref={this.editLayer}
                    style={{ width: pdfSize.width, height: pdfSize.height }}
                  >
                  {
                    inputs[pageNum - 1].map((input, i) => {
                      if (input.type === 'text') {
                        return (
                          <TextBox
                            key={input.id}
                            onChange={editorState => this.setState((state) => {
                              input.editorState = editorState;
                              return { inputs: update(state.inputs, { [pageNum - 1]: { $splice: [[i, 1, input]] } } ) };
                            })}
                            editorState={input.editorState}
                            x={input.x}
                            y={input.y}
                            width={input.width}
                            height={input.height}
                            label={input.label}
                            number={i+1}
                            readOnly
                          />
                        );
                    } else if (input.type === 'image') {
                      return (
                        <ImageBox
                          key={input.id}
                          onChange={this.onImageUpload(i)}
                          url={input.url}
                          x={input.x}
                          y={input.y}
                          width={input.width}
                          height={input.height}
                        />
                      );
                    }
                    })
                  }
                </div>
              </Fragment>
              : null
            }
            {
              isNew ?
              <canvas
                className={classes.editLayer}
                onMouseDown={this.handleEditCanvasEvents('down')}
                onMouseUp={this.handleEditCanvasEvents('up')}
                onMouseMove={this.handleEditCanvasEvents('move')}
                ref={this.editCanvas}
                id="editCanvas"
              /> : null
            }
            <canvas className={classes.pdf} ref={this.canvas} id="canvas" />
          </div>
          }
        </div>
        <Sidebar
          loading={loading}
          isMobile={isMobile}
          handleClick={this.handleSidebarClick}
          inputs={inputs[pageNum - 1] && inputs[pageNum - 1].map(o => ({
            ...o,
            textForForm: o.type === 'text' && o.editorState.getCurrentContent().getPlainText(),
          }))}
          isNew={isNew}
          onlySubmit={onlySubmit}
          onTextChange={(i, text) => {
            this.setState((state) => {
              const input = inputs[pageNum-1][i];
              input.editorState = EditorState.createWithContent(ContentState.createFromText(text));
              console.log(text);
              console.log(input);
              return { inputs: update(state.inputs, { [pageNum - 1]: { $splice: [[i, 1, input]] } } ) };
            })}
          }
          onImageChange={(i, file) => this.onImageUpload(i)(file)}
          values={{
            writer: this.state.writer,
            title: this.state.title,
          }}
          handleChange={(label) => (e) => this.setState({
            [label]: e.target.value,
          })}
          disableSubmit={this.state.title === '' || this.state.writer === ''}
          open={!isMobile || drawerOpen}
          onClose={() => this.setState({ drawerOpen: false })}
        />
        { loading && <Loader />}
      </Layout>
    );
  }
}
const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch => bindActionCreators({
  
}, dispatch);
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(Editor)));
