import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import InputAdornment from '@material-ui/core/InputAdornment';
import Icon from '@material-ui/core/Icon';
import Typo from '@material-ui/core/Typography';

// @material-ui/icons

import Email from '@material-ui/icons/Email';
// import LockOutline from '@material-ui/icons/LockOutline';

// core components
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Card from 'components/Card/Card.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';

import loginPageStyle from 'assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx';
import Loader from '../../../../components/Loader';

class LoginPage extends React.Component {
  state = {
    view: 'login',
    email: '',
    password: '',
    passwordConfirm: '',
  };
  componentWillReceiveProps(nextProps) {
    const { isSignUpSuccess } = nextProps;
    if (isSignUpSuccess) {
      this.setState({
        view: 'login',
      });
    }
  }
  viewChange = label => () => this.setState({ view: label });
  handleChange = label => e => this.setState({
    [label]: e.target.value,
  });
  render() {
    const { classes, isLoading, handleSubmit } = this.props;
    const {
      view,
      email,
      password,
      passwordConfirm,
    } = this.state;
    const isSignUp = view === 'signUp';
    return (
      <div className={classes.container}>
        <GridContainer justify='center'>
          <GridItem xs={12} sm={6} md={4}>
            <form>
              <Card login className={classes[this.state.cardAnimaton]}>
                <CardHeader
                  className={`${classes.cardHeader} ${classes.textCenter}`}
                  color='rose'
                >
                  <h2
                    className={classes.cardTitle}
                    style={{ padding : 10 }}
                  >
                  체인저
                  </h2>
                </CardHeader>
                <CardBody>
                  {
                    isSignUp ?
                    <Typo variant='h6'>
                      회원가입
                    </Typo> : null
                  }
                  <CustomInput
                    labelText='이메일'
                    id='email'
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      onChange: this.handleChange('email'),
                      value: email,
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Email className={classes.inputAdornmentIcon} />
                        </InputAdornment>
                      )
                    }}
                  />
                  <CustomInput
                    labelText='비밀번호'
                    id='password'
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      type:'password',
                      onChange: this.handleChange('password'),
                      value: password,
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Icon className={classes.inputAdornmentIcon}>
                            lock_outline
                          </Icon>
                        </InputAdornment>
                      )
                    }}
                  />
                  {
                    isSignUp ?
                    <CustomInput
                      labelText='비밀번호 확인'
                      id='passwordConfirm'
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type:'password',
                        onChange: this.handleChange('passwordConfirm'),
                        value: passwordConfirm,
                        endAdornment: (
                          <InputAdornment position='end'>
                            <Icon className={classes.inputAdornmentIcon}>
                              done
                            </Icon>
                          </InputAdornment>
                        )
                      }}
                    /> : null
                  }
                </CardBody>
                <CardFooter className={classes.justifyContentCenter}>
                  <Button
                    onClick={() => handleSubmit(this.state)}
                    color='rose'
                    simple
                    block
                    size='lg'
                  >
                    { isSignUp ? '가입하기' : '로그인' }
                  </Button>
                  <Button
                    onClick={() => this.setState({
                      view: isSignUp ? 'login' : 'signUp',
                    })}
                    color='rose'
                    simple
                    block
                    size='lg'
                  >
                    { isSignUp ? '취소' : '회원 가입' }
                  </Button>
                </CardFooter>
              </Card>
            </form>
          </GridItem>
        </GridContainer>
        { isLoading && <Loader /> }
      </div>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(loginPageStyle)(LoginPage);
