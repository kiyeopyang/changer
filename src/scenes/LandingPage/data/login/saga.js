import {
  actions,
  login,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { response, error } = yield call(
    request,
    '/api/login',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    },
  );
  if (response) {
    yield put(login.success(params, response));
    window.location.reload();
    // yield put(auth.request())
  }
  else yield put(login.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
