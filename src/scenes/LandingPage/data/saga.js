import login from './login/saga';
import signUp from './signUp/saga';

export default [
  ...login,
  ...signUp,
];
