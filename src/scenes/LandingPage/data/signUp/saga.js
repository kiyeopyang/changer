import {
  actions,
  signUp,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { email, password, passwordConfirm } = params;
  if (email === '') yield put(signUp.failure(params, '이메일을 입력하십시요.'));
  else if (password === '') yield put(signUp.failure(params, '패스워드를 입력하십시요.'));
  else if (password.length < 4) yield put(signUp.failure(params, '4글자 이상의 패스워드를 입력하십시요.'));
  else if (password !== passwordConfirm) yield put(signUp.failure(params, '패스워드 확인 값이 잘못되었습니다.'));
  else {
    const { response, error } = yield call(
      request,
      '/api/signup',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
      },
    );
    if (response) {
      yield put(signUp.success(params, response));
    }
    else yield put(signUp.failure(params, error));
  }
}
export default [
  takeLatest(actions['REQUEST'], req),
];
