import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  withRouter,
} from 'react-router-dom';
import {
  login,
} from './data/login/actions';
import {
  signUp,
} from './data/signUp/actions';
import {
  showMessage, 
} from '../../data/messageBar/actions';
import Layout from './components/Layout';
import Form from './components/Form';

class Scene extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.signUp.response) {
      this.props.showMessage({ message: '로그인을 해 주십시요.' });
    }
  }
  handleSubmit = ({ view, email, password, passwordConfirm }) => {
    const input = { email, password, passwordConfirm };
    if (view === 'login')
      this.props.loginRequest(input);
    else
      this.props.signUpRequest(input);
  }
  render() {
    const { signUp, login } = this.props;
    const isSignUpSuccess = signUp.response;
    return (
      <Layout>
        <Form
          isLoading={signUp.loading || login.loading}
          isSignUpSuccess={isSignUpSuccess}
          handleSubmit={this.handleSubmit}
        />
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  login: state.LandingPage.data.login,
  signUp: state.LandingPage.data.signUp,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  loginRequest: login.request,
  signUpRequest: signUp.request,
  showMessage,
}, dispatch);
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Scene));
