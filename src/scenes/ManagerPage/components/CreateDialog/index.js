import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from "components/CustomButtons/Button.jsx";
import TextField from '@material-ui/core/TextField';
import Typo from '@material-ui/core/Typography';

const styles = {
  fileInput: {
    display: 'none',
  },
};
class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      file: null,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.open && !nextProps.open) {
      this.setState({ title: '', file: null });
    }
  }
  handleChange = label => e => this.setState({
    [label]: e.target.value,
  });
  handleFileChange = label => e => this.setState({
    [label]: e.target.files[0],
  });
  render() {
    const { classes, open, onClose, onSubmit } = this.props;
    const { title, file } = this.state;
    return (
      <Dialog
        open={open}
        onClose={onClose}
      >
        <DialogTitle>문서 생성</DialogTitle>
        <DialogContent>
          <TextField
            fullWidth
            type="text"
            value={title}
            onChange={this.handleChange('title')}
            label="타이틀"
          />
          <input
            className={classes.fileInput}
            id="outlined-button-file"
            type="file"
            onChange={this.handleFileChange('file')}
          />
          <label htmlFor="outlined-button-file">
            <Button variant="outlined" color="rose" component="span">
              파일 업로드
            </Button>
          </label>
          {file ? <h4>{file.name}</h4> : null}
        </DialogContent>
        <DialogActions>
          <Button onClick={() => onSubmit(this.state)} color="rose">생성</Button>
          <Button onClick={() => onClose()} color="rose" simple>취소</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(Component);
