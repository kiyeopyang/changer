import React from 'react';
import cx from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';
import PerfectScrollbar from 'perfect-scrollbar';
import 'perfect-scrollbar/css/perfect-scrollbar.css';
import Sidebar from './components/Sidebar';
import appStyle from 'assets/jss/material-dashboard-pro-react/layouts/dashboardStyle.jsx';

var ps;
class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileOpen: false,
      miniActive: false,
    };// 
  }
  componentDidMount() {
    if (navigator.platform.indexOf('Win') > -1) {
      ps = new PerfectScrollbar(this.refs.mainPanel, {
        suppressScrollX: true,
        suppressScrollY: false
      });
      document.body.style.overflow = 'hidden';
    }
    window.addEventListener('resize', this.resizeFunction);
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf('Win') > -1) {
      ps.destroy();
    }
    window.removeEventListener('resize', this.resizeFunction);
  }
  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  getRoute = () => {
    return this.props.location.pathname !== '/maps/full-screen-maps';
  }
  sidebarMinimize = () => {
    this.setState({ miniActive: !this.state.miniActive });
  }
  resizeFunction = () => {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  }
  render() {
    const { classes, children, routes, user, documents, selected, ...rest } = this.props;
    const mainPanel =
      classes.mainPanel +
      ' ' +
      cx({
        [classes.mainPanelSidebarMini]: this.state.miniActive,
        [classes.mainPanelWithPerfectScrollbar]:
          navigator.platform.indexOf('Win') > -1
      });
    return (
      <div className={classes.wrapper}>
        <Sidebar
          user={user}
          // routes={routes}
          routes={documents}
          selected={selected}
          logoText={'체인저'}
          email={user.email}
          handleDrawerToggle={this.handleDrawerToggle}
          color="white"
          bgColor="black"
          open={this.state.mobileOpen}
          miniActive={this.state.miniActive}
          handleClick={this.props.handleSidebarClick}
          {...rest}
        />
        <div className={mainPanel} ref='mainPanel'>
          {children}
        </div>
      </div>
    );
  }
}
export default withStyles(appStyle)(Component);
