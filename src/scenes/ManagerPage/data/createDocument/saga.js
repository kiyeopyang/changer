import {
  actions,
  createDocument,
} from './actions';
import {
  documents,
} from '../documents/actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { file, title } = params;
  if (!file) yield put(createDocument.failure(params, '파일을 추가하십시요.'));
  else if (!title || title === '') yield put(createDocument.failure(params, '유효한 타이틀을 입력하십시요.'));
  else {
    const data = new FormData();
    data.append('file', file);
    data.append('title', title);
    const { response, error } = yield call(
      request,
      '/api/document',
      {
        method: 'POST',
        headers: {
          credentials: 'include',
        },
        body: data,
      },
    );
    if (response) {
      yield put(documents.request());
      yield put(createDocument.success(params, response));
    }
    else yield put(createDocument.failure(params, error));
  }
}
export default [
  takeLatest(actions['REQUEST'], req),
];
