import {
  actions,
  deleteDocument,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { no } = params;
  const { response, error } = yield call(
    request,
    `/api/document/${no}`,
    {
      method: 'DELETE',
      headers: {
        credentials: 'include',
      },
    },
  );
  if (response) {
    // yield put(documents.request());
    window.location.reload();
    yield put(deleteDocument.success(params, response));
  }
  else yield put(deleteDocument.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
