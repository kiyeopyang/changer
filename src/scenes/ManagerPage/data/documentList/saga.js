import {
  actions,
  documentList,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { response, error } = yield call(
    request,
    '/api/documentlist',
    {
      headers: {
        credentials: 'include',
      },
    },
  );
  if (response) {
    yield put(documentList.success(params, response));
  }
  else yield put(documentList.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
