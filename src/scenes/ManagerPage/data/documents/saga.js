import {
  actions,
  documents,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { response, error } = yield call(
    request,
    '/api/document',
    {
      headers: {
        credentials: 'include',
      },
    },
  );
  if (response) {
    yield put(documents.success(params, response));
  }
  else yield put(documents.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
