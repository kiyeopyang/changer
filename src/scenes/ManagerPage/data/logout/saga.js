import {
  actions,
  logout,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { response, error } = yield call(
    request,
    '/api/logout',
    {
      method: 'POST',
      headers: {
        credentials: 'include',
      },
    },
  );
  if (response) {
    yield put(logout.success(params, response));
    window.location.href = '/'
  }
  else yield put(logout.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
