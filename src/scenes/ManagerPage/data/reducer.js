import { combineReducers } from 'redux';
import documents from './documents/reducer';
import createDocument from './createDocument/reducer';
import deleteDocument from './deleteDocument/reducer';
import logout from './logout/reducer';
import documentList from './documentList/reducer';

export default combineReducers({
  documents,
  createDocument,
  deleteDocument,
  logout,
  documentList,
});
