import documents from './documents/saga';
import createDocument from './createDocument/saga';
import deleteDocument from './deleteDocument/saga';
import logout from './logout/saga';
import documentList from './documentList/saga';

export default [
  ...documents,
  ...createDocument,
  ...deleteDocument,
  ...logout,
  ...documentList,
];
