import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  withRouter,
} from 'react-router-dom';
import { documents } from './data/documents/actions';
import { createDocument } from './data/createDocument/actions';
import { deleteDocument } from './data/deleteDocument/actions';
import { logout } from './data/logout/actions';
import { documentList } from './data/documentList/actions';
import Editor from './scenes/ManagerEditor';
import routes from './routes';
import Layout from './components/Layout';
import CreateDialog from './components/CreateDialog';
import Loader from '../../components/Loader';

class Scene extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null,
      createDialogOn: false,
      lastSelectedTime: new Date().getTime(),
    };
    this.props.documentsRequest();
    this.props.documentListRequest();
  }
  handleClick = (label, v) => {
    switch (label) {
      case 'create': {
        this.setState({
          createDialogOn: true,
        });
        break;
      }
      case 'logout': {
        this.props.logoutRequest();
        break;
      }
      case 'delete': {
        this.props.deleteDocumentRequest({
          no: this.state.selected.no,
        });
        this.setState({ selected: null });
        break;
      }
      case 'doc': {
        const { lastSelectedTime } = this.state;
        let now = new Date().getTime();
        if (now - lastSelectedTime > 500) {
          this.setState({
            selected: v,
            lastSelectedTime: now,
          });
        }
        break;
      }
      default:
    }
  }
  handleCreate = (input) => {
    this.setState({ createDialogOn: false });
    this.props.createDocumentRequest(input);
  }
  render() {
    const { auth, documents, createDocument, documentList, ...rest } = this.props;
    const { selected, createDialogOn } = this.state;
    let docs = documents.response || [];
    let docList = documentList.response || [];
    const isLoading = documents.loading || createDocument.loading || documentList.loading ;
    docs = docs.map((o) => {
      const found = docList.find(j => j.no === o.no);
      return ({
        ...o,
        others: found ? found.docDataList : null,
      });
    });
    console.log(selected);
    return (
      <Layout
        user={auth.response}
        routes={routes}
        documents={docs}
        selected={selected}
        handleSidebarClick={this.handleClick}
        {...rest}
      >
        {
          selected ?
            <Editor
              document={docs.find(o => o.no === selected.no)}
              handleClick={this.handleClick}
            />
            : null
        }
        <CreateDialog
          open={createDialogOn}
          onClose={() => this.setState({ createDialogOn: false })}
          onSubmit={this.handleCreate}
        />
        {isLoading && <Loader />}
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  auth: state.data.auth,
  documents: state.ManagerPage.data.documents,
  createDocument: state.ManagerPage.data.createDocument,
  deleteDocument: state.ManagerPage.data.deleteDocument,
  documentList: state.ManagerPage.data.documentList,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  documentsRequest: documents.request,
  createDocumentRequest: createDocument.request,
  deleteDocumentRequest: deleteDocument.request,
  logoutRequest: logout.request,
  documentListRequest: documentList.request,
}, dispatch);
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Scene));
