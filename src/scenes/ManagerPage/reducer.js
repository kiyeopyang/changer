import { combineReducers } from 'redux';
import data from './data/reducer';
import ManagerEditor from './scenes/ManagerEditor/reducer';

export default combineReducers({
  data,
  ManagerEditor,
});
