import Test from './scenes/Test';

// @material-ui/icons
import DashboardIcon from "@material-ui/icons/Dashboard";
import Image from "@material-ui/icons/Image";

var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: DashboardIcon,
    component: Test,
  },
  {
    collapse: true,
    path: "-page",
    name: "Pages",
    state: "openPages",
    icon: Image,
    views: [
      {
        path: "/timeline-page",
        name: "Timeline Page",
        mini: "TP",
        component: Test,
      },
      {
        path: "/user-page",
        name: "User Profile",
        mini: "UP",
        component: Test,
      },
      {
        path: "/rtl/rtl-support-page",
        name: "RTL Support",
        mini: "RS",
        component: Test,
      }
    ]
  },
];
export default routes;
