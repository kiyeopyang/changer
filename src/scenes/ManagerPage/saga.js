import saga from './data/saga';
import ManagerEditor from './scenes/ManagerEditor/saga';

export default [
  ...saga,
  ...ManagerEditor,
];
