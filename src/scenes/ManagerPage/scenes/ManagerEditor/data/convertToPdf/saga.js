import {
  actions,
  convertToPdf,
} from './actions';
import request from '../../../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { response, error } = yield call(
    request,
    `/api/download`,
    {
      method: 'POST',
      headers: {
        credentials: 'include',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    },
  );
  if (response) {
    console.log(response.url);
    window.open(`https://forjeeun.xyz${response.url}`);
    yield put(convertToPdf.success(params, response));
  }
  else yield put(convertToPdf.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
