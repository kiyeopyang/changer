import {
  actions,
  createForms,
} from './actions';
import {
  documents,
} from '../../../../data/documents/actions';
import request from '../../../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { no, ...rest } = params;
  const { response, error } = yield call(
    request,
    `/api/form/${no}`,
    {
      method: 'POST',
      headers: {
        credentials: 'include',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(rest),
    },
  );
  if (response) {
    yield put(documents.request());
    yield put(createForms.success(params, response));
  }
  else yield put(createForms.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
