import {
  actions,
  getSubmitted,
} from './actions';
import request from '../../../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* req({ params }) {
  const { no, documentNo } = params;
  const { response, error } = yield call(
    request,
    `/api/document/data/${no}`,
    {
      method: 'GET',
      headers: {
        credentials: 'include',
      },
    },
  );
  if (response) {
    yield put(getSubmitted.success(params, response));
  }
  else yield put(getSubmitted.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], req),
];
