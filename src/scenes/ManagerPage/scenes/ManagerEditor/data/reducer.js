import { combineReducers } from 'redux';
import createForms from './createForms/reducer';
import convertToPdf from './convertToPdf/reducer';
import getSubmitted from './getSubmitted/reducer';

export default combineReducers({
  createForms,
  convertToPdf,
  getSubmitted,
});
