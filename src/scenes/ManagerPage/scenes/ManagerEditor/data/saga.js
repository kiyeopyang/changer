import createForms from './createForms/saga';
import convertToPdf from './convertToPdf/saga';
import getSubmitted from './getSubmitted/saga';

export default [
  ...createForms,
  ...convertToPdf,
  ...getSubmitted,
];
