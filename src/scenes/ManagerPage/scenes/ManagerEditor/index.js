import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  withRouter,
} from 'react-router-dom';
import copy from 'copy-to-clipboard';
import Layout from './components/Layout';
import Editor from '../../../Editor';
import Loader from '../../../../components/Loader';
import { createForms } from './data/createForms/actions';
import { convertToPdf } from './data/convertToPdf/actions';
import { getSubmitted } from './data/getSubmitted/actions';

class Scene extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedSubmitted: null,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.document !== nextProps.document) {
      this.setState({
        selectedSubmitted: null,
      });
    } else {
      if (this.props.getSubmitted.response !== nextProps.getSubmitted.response) {
        this.setState({
          selectedSubmitted: nextProps.getSubmitted.response,
        });
      }
    }
  }
  handleChange = label => e => this.setState({
    [label]: e.target.value,
  });
  handleFileChange = label => e => this.setState({
    [label]: e.target.files[0],
  });
  createDocumentRequest = () => {
    const { title, file } = this.state;
    const data = new FormData();
    data.append('file', file);
    data.append('title', title);
    this.props.createDocumentRequest(data);
  };
  createFormsRequest = (props) => {
    console.log(props);
  };
  handleEditorClick = (label, props) => {
    const {
      document,
      createFormsRequest,
      convertToPdfRequest,
    } = this.props;
    switch(label) {
      case 'pdf': 
        convertToPdfRequest({
          ...props,
          filename: document.filename,
        });
        break;
      case 'save': 
        createFormsRequest({
          forms: props,
          no: document.no,
        });
        break;
      case 'copy':
        const url = `https://forjeeun.xyz/for-submit/${document.no}`;
        copy(url);
        window.open(url);
        break;
      case 'submit':
        this.props.getSubmittedRequest(props);
        break;
      default:
        this.props.handleClick(label, props);
    }
  }
  render() {
    const { document, convertToPdf, getSubmitted } = this.props;
    const { selectedSubmitted } = this.state;
    return (
      <Layout>
        {
          document ?
          <Editor
            pdfUrl={`/file/${document.filename}`}
            forms={document.forms}
            isNew={document.forms.length === 0}
            handleClick={this.handleEditorClick}
            onSubmit={this.createFormsRequest}
            submitted={document.others}
            submittedItem={selectedSubmitted && selectedSubmitted.forms}
            submittedItemLoading={getSubmitted.loading}
          /> : null
        }
        {
          convertToPdf.loading ?
          <Loader /> : null
        }
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  auth: state.data.auth,
  createForms: state.ManagerPage.ManagerEditor.data.createForms,
  convertToPdf: state.ManagerPage.ManagerEditor.data.convertToPdf,
  getSubmitted: state.ManagerPage.ManagerEditor.data.getSubmitted,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  createFormsRequest: createForms.request,
  convertToPdfRequest: convertToPdf.request,
  getSubmittedRequest: getSubmitted.request,
}, dispatch);
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Scene));
