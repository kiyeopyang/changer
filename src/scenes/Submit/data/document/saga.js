import {
  actions,
  document,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* get({ params }) {
  const { response, error } = yield call(
    request,
    `/api/document/${params.id}`, // no
    );
  console.log(response);
  if (response) yield put(document.success(params, response));
  else yield put(document.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], get),
];
