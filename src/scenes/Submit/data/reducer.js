import { combineReducers } from 'redux';
import document from './document/reducer';
import submit from './submit/reducer';

export default combineReducers({
  document,
  submit,
});
