import document from './document/saga';
import submit from './submit/saga';

export default [
  ...document,
  ...submit,
];
