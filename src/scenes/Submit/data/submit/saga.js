import {
  actions,
  submit,
} from './actions';
import request from '../../../../modules/request';

import {
  put,
  takeLatest,
  call,
} from 'redux-saga/effects';

function* get({ params }) {
  const {no, ...rest} = params;
  console.log(rest);
  const { response, error } = yield call(
    request,
    `/api/document/submit/${no}`, // no
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(rest),
    },
  );
  if (response) yield put(submit.success(params, response));
  else yield put(submit.failure(params, error));
}
export default [
  takeLatest(actions['REQUEST'], get),
];
