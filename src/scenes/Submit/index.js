import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';
import {
  Route,
  Switch,
  Redirect,
  withRouter,
} from 'react-router-dom';
import {
  document,
} from './data/document/actions';
import {
  submit,
} from './data/submit/actions';
import Layout from './components/Layout';
import Editor from '../EditorForSubmit';
import SubmitDialog from './components/SubmitDialog';

class Scene extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitDialogOn: false,
    };
    const { params } = this.props.match;
    const { id } = params;
    this.props.documentRequest({ id });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.submit.response) {
      this.setState({
        submitDialogOn: true,
      });
    }
  }
  handleEditorClick = (label, v) => {
    switch (label) {
      case 'submit': {
        const { title, writer, forms } = v;
        const no = this.props.match.params.id;
        this.props.submitRequest({
          title,
          writer,
          forms,
          no,
        });
        break;
      }
      default:
    }
  }
  onSubmit = (v) => {
    console.log(v);
    this.setState({
      submitDialogOn: true,
    });
  }
  render() {
    const { document } = this.props;
    const { submitDialogOn } = this.state;
    const doc = document.response;
    console.log(doc);
    return (
      <Layout>
        {
          doc ?
          <Editor
            onlySubmit
            pdfUrl={`/file/${doc.filename}`}
            forms={doc.forms}
            handleClick={this.handleEditorClick}
            onSubmit={this.onSubmit}
          /> : null
        }
        <SubmitDialog
          open={submitDialogOn}
          onClose={() => window.location.reload()}
        />
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  document: state.Submit.data.document,
  submit: state.Submit.data.submit,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  documentRequest: document.request,
  submitRequest: submit.request,
}, dispatch);
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Scene));
